# TUTORIAL: Typenssicherheit mit `Kotlin` in `Javascript` (RIP `Typescript`)

Ich habe versucht mit Typescript unter Ubuntu 16.04 ein Projekt mit NodeJS und Express aufzusetzen. Dabei ist nach mehreren Rumprobieren und Probleme beheben nichts rausgekommen. Daher habe ich mich entschieden es mit dem `Kotlin to Javascript` zu probieren. Und es klappte ohne Probleme. Nachfolgend das Resultat.

# Benutzte Software
https://yarnpkg.com/lang/en/
- yarn prüfen: `$ yarn --version`
    - Ausgabe: `1.3.2`


- gradle prüfen: `$ gradle --version`
    - Ausgabe: 
 
 ```

------------------------------------------------------------
Gradle 2.10
------------------------------------------------------------

Build time:   2016-01-26 15:17:49 UTC
Build number: none
Revision:     UNKNOWN

Groovy:       2.4.5
Ant:          Apache Ant(TM) version 1.9.6 compiled on July 8 2015
JVM:          1.8.0_151 (Oracle Corporation 25.151-b12)
OS:       

```

- Kotlin wurde mit InelliJ installiert

# Kap. I - Projekt `kotlinjsnodeexpress` sofort ausprobieren

```
git clone git@gitlab.com:vxv/kotlinjsnodeexpress.git
cd kotlinjsnodeexpress
yarn install
gradle build
yarn start
```
Wenn alle Befehle erfolgreich dann im Browser `http://localhost:3000` besuchen.


# Kap. II - Weiterentwicklung des Projekts `kotlinjsnodeexpress` mit IntelliJ IDEA

## Vorbereitung
1. `$ git clone git@gitlab.com:vxv/kotlinjsnodeexpress.git`
2. `$ cd kotlinjsnodeexpress`
3. `$ yarn && gradle build`

## In IntelliJ IDEA öffnen

1. Welcome to IntelliJ Idea Bildschirm -> Import Project ->  Ordner `kotlinjsnodeexpress` auswählen -> Ok -> Create project from existing sources -> Next -> Next -> Next -> Finish
	- (Alternativ: File -> Project from existing sources -> Ordner `kotlinjsnodeexpress` auswählen -> Ok -> Create project from existing sources -> -> Next -> Next -> Next -> Finish)

## Konfigurieren für Run und Debug

1. Im Projekt Ordner -> `myapp/index.js` öffnen -> Rechtsklick in der geöffneten Datei -> Debug 'index.js'
2. Obere Menü -> Run -> Edit configurations -> Before launch: Activate tool window -> grüner Plus Button -> Run gradle task -> Gradle Project Ordner `kotlinjsnodeexpress` auswählen -> Tasks: `build` eintragen -> Ok -> Ok

## Quellcode ändern und Ausprobieren

1 Öffne  `src/main/kotlin/Main.kt` ändere Zeile 6 `println("Hello JavaScript!")` auf `println("Hello von KotlinJS!")`
2 Obere Menü -> Run -> Run 'index.js'. Die Ausgabe in der Konsole ist:

```

Hello von KotlinJS!
Visit: http://localhost:3000
```

---
---
---

# Kap. III - Step by Step Tutorial Projekt `knode` zum nachvollziehen wie das obere Projekt `kotlinjsnodeexpress` entstanden ist.

## Vorbereitung

1. `$ mkdir knode`

2. `$ cd knode`

3. `$ yarn init`

4. `$ yarn add express`

5. `$ yarn add kotlin`


## Anlegen des Buildscripts

1. `$ touch build.gradle`

2. Inhalt der Datei: `build.gradle`

```groovy
// build.gradle
group 'com.mydomain.www'
version '1.0-SNAPSHOT'

buildscript {
	ext.kotlin_version = '1.2.10'
	repositories {
		mavenCentral()
	}
	dependencies {
		classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
	}
}

apply plugin: 'kotlin2js'

repositories {
	mavenCentral()
}

dependencies {
	compile "org.jetbrains.kotlin:kotlin-stdlib-js:$kotlin_version"
}

compileKotlin2Js.kotlinOptions {
	moduleKind = "commonjs"
	outputFile = "myapp/index.js"
	sourceMap = true
}
```
## Anlegen der Verzeichnisstruktur und der `Main.kt`

1. `$ mkdir -p src/main/kotlin`

2. `$ touch src/main/kotlin/Main.kt`

3. Inhalt der Datei: `Main.kt`

```kotlin
// Main.kt
external fun require(module:String):dynamic

fun main(args: Array<String>) {

    println("Hello JavaScript!")

    val express = require("express")
    val app = express()

    app.get("/", { _, res ->
        res.type("text/plain")
        res.send("Hello World from Kotlin with ExpressJS!")
    })

	app.listen(3000, {
        println("Visit: http://localhost:3000")
    })
}
```

## Verändern der Datei `package.json`

### packages.json VORHER

Inhalt von `package.json` **VORHER**:
```json
{
	"name": "knode",
	"version": "1.0.0",
	"main": "index.js",
	"license": "MIT",
	"dependencies": {
		"express": "^4.16.2",
		"kotlin": "^1.2.10"
	}
}
```

### packages.json NACHHER

Inhalt von `package.json` **NACHHER**:
```json
{
	"name": "knode",
	"version": "1.0.0",
	"main": "index.js",
	"license": "MIT",
	"scripts": {
		"start": "node myapp/index.js"
	},
	"dependencies": {
		"express": "^4.16.2",
		"kotlin": "^1.2.10"
	}
}
```

## Projekt bauen und starten

1. `$ gradle build`

2. `$ yarn start`

3. Öffne im Browser die angegebene URL und du siehst folgenden Text: 

	`Hello World from Kotlin with ExpressJS!`
