// Main.kt
external fun require(module:String):dynamic

fun main(args: Array<String>) {

    println("Hello JavaScript!")

    val express = require("express")
    val app = express()

    app.get("/", { _, res ->
        res.type("text/plain")
        res.send("Hello World from Kotlin with ExpressJS!")
    })

    app.listen(3000, {
        println("Visit: http://localhost:3000")
    })

}
